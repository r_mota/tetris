<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Theme_model extends CI_Model {

	 function __construct()
    {
        parent::__construct();
        #$this->load->helper("");
		//echo "Hello Model";

    }

    public function insert_theme($name, $image, $music){
    	$data = array(
    		'theme_name' =>$name,
    		'img_path' =>$image,
    		'music_path' =>$music,
    		);

    	$this->db->insert('theme',$data);
    }

    public function get_themes(){
        $query = $this->db->get('theme');
        return $query->result();
    }

    public function update_theme($username, $img_path){
        $this->db->select('id');
        $this->db->from('theme');
        $this->db->where('img_path', "themes/image/".$img_path);

        $query = $this->db->get();

        $result = $query->result();
        
        $data = array("theme_id" => $result[0]->id);
        $this->db->where("nombreusuario",$username);
        $this->db->update("users",$data);
    }

    public function get_theme_user($username){
        $this->db->select("img_path");
        $this->db->from("users");
        $this->db->join("theme","theme_id = id","inner");
        $this->db->where("nombreusuario",$username);

        $query = $this->db->get();
        return $query->result(); 
    }

}    