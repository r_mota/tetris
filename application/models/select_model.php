<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Select_model extends CI_Model {

	 function __construct()
    {
        parent::__construct();
        #$this->load->helper("");
		//echo "Hello Model";
    }


      public function select(){
    
		
		$this->db->select('NombreUsuario');
		$this->db->select_max('Puntaje');
		$this->db->from ('nivel');
		$this->db->group_by('NombreUsuario'); 

		$this->db->order_by("Puntaje", "desc");
		$this->db->limit(10);

		
		$query = $this->db->get();
    	
		
    	return $query-> result();

    }
	
}