<!DOCTYPE html>
 <html lang="en" class="no-js"> 
    <head>
        <meta charset="UTF-8" />
        
        <link rel="shortcut icon" href="../favicon.ico"/> 
        <link rel="stylesheet" type="text/css" href="/css/style.css" />
		<link rel="stylesheet" type="text/css" href="/css/animate-custom.css" />
    </head>
    <body>
        <div class="container">
            <section>	
				<div id="container_demo" >
					<a class="hiddenanchor" id="toregister"></a>
                    <div id="wrapper">
                        <div id="login" class="animate form">
                            <form  method="post" autocomplete="on">
                                <h1> Register </h1> 
                                <p> 
                                    <?php if (isset($error)):?> 
                                        <center>
                                            <div class="error_label">
                                                <?php echo $error;?>
                                            </div>
                                        </center>
                                    <?php endif;?>    
                                    <label for="usernamesignup" class="uname" data-icon="u">Username</label>
                                    <?php 
                                        if(isset($username)):
                                    ?>
                                        <input id="usernamesignup" name="usernamesignup" required="required" type="text" placeholder="mysuperusername690" value="<?php echo $username;?>"/>
                                    <?php 
                                        else:
                                    ?>
                                        <input id="usernamesignup" name="usernamesignup" required="required" type="text" placeholder="mysuperusername690" />
                                    <?php endif;?>    
                                </p>
                                <p> 
                                    <label for="emailsignup" class="youmail" data-icon="e" > Email</label>
                                    <?php 
                                        if(isset($email)):
                                    ?>
                                        <input id="emailsignup" name="emailsignup" required="required" type="email" placeholder="mysupermail@mail.com" value="<?php echo $email;?>"/> 
                                    <?php
                                        else:
                                    ?>
                                        <input id="emailsignup" name="emailsignup" required="required" type="email" placeholder="mysupermail@mail.com"/> 
                                    <?php endif;?>    
                                </p>
                                <p> 
                                    <label for="passwordsignup" class="youpasswd" data-icon="p">Password</label>
                                    <input id="passwordsignup" name="passwordsignup" required="required" type="password" placeholder="eg. X8df!90EO"/>
                                </p>
                                <p> 
                                    <label for="passwordsignup_confirm" class="youpasswd" data-icon="p">Confirm password </label>
                                    <input id="passwordsignup_confirm" name="passwordsignup_confirm" required="required" type="password" placeholder="eg. X8df!90EO"/>
                                </p>
                                <p class="signin button"> 
									<input type="submit" value="Register"/> 
								</p>
                                <p class="change_link">  
									Are you registered?
									<a href="/auth/login" class="to_register"> Login </a>
								</p>
                            </form>
                        </div>
                    </div>
				</div>
            </section>
        </div>
    </body>
</html>