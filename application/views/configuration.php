<!DOCTYPE html>
 <html lang="en" class="no-js"> 
    <head>
        <meta charset="UTF-8" />
        
        <link rel="shortcut icon" href="../favicon.ico"> 
        <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" />
      <!--  <link rel="stylesheet" type="text/css" href="/css/style.css" />
		<link rel="stylesheet" type="text/css" href="/css/animate-custom.css" />-->
    </head>
    <body>
        <div class="header">
            <div class= "navbar navbar-default navbar-fixed-top">
                <div class="container">
                    <input type="hidden" id="username" value="<?php echo $username?> "/>
                    <a class="navbar-brand" href="/">Tetrissoft</a>
                    <ul class="nav navbar-nav">
                        <li><a href="/">Home</a></li>
                        <li><a href="/scores">Ranking</a></li>
                        <li><a href="/user_scores">Scores</a></li>
                        <li class="active"><a href="/config">Config</a></li>
                        <?php if ($admin ==1):?> 
                            <li><a href="admin">Themes</a></li>
                        <?php endif;?> 
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        
                        <li><a href="auth/logout"> <?php echo $username.", logout"?> </a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container">
            <br>
            <br>
            <div class="col-md-4 col-md-offset-3">
                <center> <h1>Themes</h1></center>
                <table class="table table-stripped">
                    <thead>
                        <tr>
                            <th> Theme</th>
                            <th> Select</th>
                        </tr>   
                    </thead>
                    <tbody>
                        <?php foreach ($themes as $theme):?>
                            <tr>
                                <td><?php echo $theme->theme_name; ?></td>
                                <td> <button 
                                        value ="<?php echo substr($theme->img_path, 
                                        strripos($theme->img_path, '/')+1);?>"
                                        class="btn btn-primary" href="#"> Ver
                                    </button>
                                </td>
                            </tr>

                        <?php endforeach;?>
                    </tbody>
                </table>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <canvas id="preview" width="250" height="500">
                        <p>Su navegador no soporta HTML5</p>
                    </canvas>
                    <button class="btn bnt-primary" id="save" style="display:none"> Save</button>
                </div>
            </div>
        </div>
    </body>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/theme.js"></script>
</html>