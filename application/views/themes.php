<!DOCTYPE html>
 <html lang="en" class="no-js"> 
    <head>
        <meta charset="UTF-8" />
        
        <link rel="shortcut icon" href="../favicon.ico"> 
        <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" />
      <!--  <link rel="stylesheet" type="text/css" href="/css/style.css" />
		<link rel="stylesheet" type="text/css" href="/css/animate-custom.css" />-->
    </head>
    <body>
        <div class="header">
            <div class= "navbar navbar-default navbar-fixed-top">
                <div class="container">
                    <a class="navbar-brand" href="/">Tetrissoft</a>
                    <ul class="nav navbar-nav">
                        <li><a href="/">Home</a></li>
                        <li><a href="/scores">Ranking</a></li>
                        <li><a href="/user_scores">Scores</a></li>
                        <li><a href="/config">Config</a></li>
                        <?php if ($admin ==1):?> 
                            <li class="active"><a href="admin">Themes</a></li>
                        <?php endif;?> 
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        
                        <li><a href="auth/logout"> <?php echo $username.", logout"?> </a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container">
            <div class=" col-md-5 col-md-offset-3">
                <br>
                <br>
                <br>
                <br>
                <br>
                <?php if (isset($message)):?>
                    <p>
                        <?php echo $message; ?>
                    </p>
                    <a href = "/admin" class="btn btn-primary"> Upload another</a>
                <?php else: ?>    
                <form role="form" method="POST" action="admin/upload_theme" enctype= "multipart/form-data">
                        
                    <div class="form-group">
                        <label for="theme">Theme name</label>
                        <input type="text" class="form-control" id="theme" name ="theme" placeholder="Enter theme name">
                    </div>
                    <div class="form-group">
                        <label for="image">Wallpaper</label>
                        <input type="file" class="form-control" name="image">
                    </div>
                    <div class="form-group">
                        <label for="music">Music</label>
                        <input type="file" class="form-control" name="music">
                    </div>  
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
                <?php endif?>    
            </div>
        </div>
    </body>
</html>