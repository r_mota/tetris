 <html>
	<head>
	<link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" />
	</head>
	<body>
		<div class="header">
			<div class= "navbar navbar-default navbar-fixed-top">
				<div class="container">
					<a class="navbar-brand" href="/">Tetrissoft</a>
					<ul class="nav navbar-nav">
			            <li class="active"><a href="/">Home</a></li>
			            <li><a href="/scores">Ranking</a></li>
			            <li><a href="/user_scores">Scores</a></li>
			            <li><a href="/config">Config</a></li>
			            <?php if ($admin ==1):?> 
			            	<li><a href="admin">Themes</a></li>
                        <?php endif;?> 
			        </ul>
					<ul class="nav navbar-nav navbar-right">
			            
			            <li><a href="auth/logout"> <?php echo $username.", logout"?> </a></li>
			        </ul>
				</div>
			</div>
		</div>
		<div class="container theme-showcase">
			<input type="hidden" id="username" value="<?php echo $username?> "/>
			<input type="hidden" id="path" value="<?php echo $path?> "/>
			<div class="row">
				<div class="col-md-4 col-md-offset-4">
					<br>
					<br>
					<canvas id="board" width="250" height="500">
						<p>Su navegador no soporta HTML5</p>
					</canvas>
					<div style="position:relative;top:50%">
						<br>
						<button id="start" type="button"  class="btn btn-primary btn-lg" >Start</button>
					</div>
				</div>
			</div>
		</div>	
	</body>       
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/global.js"></script>
	<script type="text/javascript" src="js/tetromino.js"></script>
	<script type="text/javascript" src="js/init.js"></script>
	<script type="text/javascript" src="js/draw.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
	<script type="text/javascript" src="js/controller.js"></script>
</html>
