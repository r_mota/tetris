<!DOCTYPE html>
 <html lang="en" class="no-js"> 
    <head>
        <meta charset="UTF-8" />
        
        <link rel="shortcut icon" href="../favicon.ico"> 
    	<link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" />  
    </head>
    <body>
    	<div class="header">
			<div class= "navbar navbar-default navbar-fixed-top">
				<div class="container">
					<a class="navbar-brand" href="/">Tetrissoft</a>
					<ul class="nav navbar-nav">
			            <li><a href="/">Home</a></li>
			            <li><a href="/scores">Ranking</a></li>
			            <li class="active"><a href="/user_scores">Scores</a></li>
						<li><a href="/config">Config</a></li>
			            <?php if ($admin ==1):?> 
			            	<li><a href="admin">Themes</a></li>
                        <?php endif;?> 
			        </ul>
					<ul class="nav navbar-nav navbar-right">
			            
			            <li><a href="auth/logout"> <?php echo $username.", logout"?> </a></li>
			        </ul>
				</div>
			</div>
		</div>
		<br>
		<br>
        <div class="container">	
        	<div class="col-md-5 col-md-offset-3">
				<center>
					<h1>My scores</h1>
				</center>
	          
				<table class="table table-striped">
					<thead>
					<tr>
						<td>
							Username 
						</td>
						<td> 
							Score
						</td>
						<td> 
							Date
						</td>
					</tr>
					</thead>
					<tbody>
					<?php foreach ($result as $var) {
						$username = $var -> NombreUsuario;
						$puntaje = $var -> Puntaje;
						$date = $var -> Fecha;
						?>
						<tr>
							<td width="150px"> 
							<?php echo $username; ?>
							</td>
							<td>
							<?php echo $puntaje; ?>
							</td> 
							<td>
							<?php echo $date; ?>
							</td> 
						</tr>															
						<?php
					}
					?>
					</tbody>
				</table>
        	</div>
        </div>
    </body>
</html>