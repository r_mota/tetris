<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class user_Scores extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("user_select_model");
		session_start();
		if(!isset($_SESSION['logged_in'])){
			redirect('/auth/login');
		}
	}


	public function index(){
		//echo("index");
		$username = $_SESSION['username'];
		$result['error'] = "";
		$result = $this->user_select_model->select($username);
		//$this->load->view('user_scores',$result);
		$data['result'] = $result;
		$data['username'] = $username;
		$data['admin']= $_SESSION['admin'];
		$this->load->view("user_scores",$data);

		
		//var_dump($result);
		//foreach ($result as $var){
		//var_dump($var);
		//}
	}

}

