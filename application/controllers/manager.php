<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manager extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();
		//print_r($_SESSION);
	}

	public function index()
	{	
		$data = $_POST['data'];
		$username = $data['username'];
		$score = $data['score'];
		$level = $data['level'];
		log_message("ERROR", str_replace("\n", "", var_export($_POST,true)));
		$this->load->model('score_model');
		$this->score_model->insert($username, $level, $score);
		#echo var_export($data,true);
		
	}

	public function theme(){
		$this->load->model("theme_model");
		$data = $_POST['data'];
		log_message("ERROR",str_replace("\n", "", print_r($_POST['data'],true)));
		$username = $data['username'];
		$img_path = $data['value'];
		log_message('ERROR', 'before theme model');
		$this->theme_model->update_theme($username,$img_path);
		log_message('ERROR', 'after theme model');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */