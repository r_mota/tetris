<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Config extends CI_Controller {

	function __construct(){
		parent::__construct();
		session_start();
		if(!isset($_SESSION['logged_in'])){
			redirect('/auth/login');
		}
	}

	public function index(){
		$this->load->model("theme_model");

		$themes = $this->theme_model->get_themes();
		

		$data['username']= $_SESSION['username'];
		$data['admin']= $_SESSION['admin'];
		$data['themes'] = $themes;
		
		$this->load->view("configuration",$data);
	}
}