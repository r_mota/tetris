<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();
		session_start();
		//print_r($_SESSION);
	}

	public function index()
	{	
		if(!isset($_SESSION['logged_in'])){
			redirect('/auth/login');
		}
		//var_dump($_SESSION);
		$this->load->model("theme_model");
		$theme = $this->theme_model->get_theme_user($_SESSION['username']);
		var_dump($theme);
		$theme =$theme[0]->img_path;
		$data['username']= $_SESSION['username'];
		$data['admin']= $_SESSION['admin'];
		$data["path"] = $theme;
		$this->load->view('index',$data);
	}

	public function save()
	{	
		$data = $_POST;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */