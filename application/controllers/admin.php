<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("theme_model");
		session_start();
		if($_SESSION["admin"]!=1){
			redirect("/");
		}
	}

	public function index(){
		$data['username']= $_SESSION['username'];
		$data['admin']= $_SESSION['admin'];
		$this->load->view("themes",$data);
	}

	public function upload_theme(){
		$name = $_POST['theme'];
		$image = "";
		$music = "";
		foreach ($_FILES as $key => $value) {
		//	print $key.">>";
			$extension = substr($value["name"], strripos($value["name"], "."));
			move_uploaded_file($value["tmp_name"],
		    "themes/".$key."/".time().$extension);
		    if($key == "image")
		    	$image = "themes/".$key."/".time().$extension;
		    else
		    	$music = "themes/".$key."/".time().$extension;
				
		}
		$this->theme_model->insert_theme($name,$image,$music);
		$data['username']= $_SESSION['username'];
		$data['admin']= $_SESSION['admin'];
		$data['message']= "Theme uploaded successfully!";
		$this->load->view("themes",$data);
	}
}