<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("auth_model");
		session_start();
		
	}

	public function index(){
		redirect('/auth/login');
	}
	
	public function register()
	{
		if(isset($_SESSION['logged_in'])){
			redirect('/');
		}
		if(isset($_POST["usernamesignup"])){## si se submit del form
			$username = $_POST['usernamesignup'];
			$email = $_POST['emailsignup'];
			$password = $_POST['passwordsignup'];
			$password2 = $_POST['passwordsignup_confirm'];


			if($password != $password2){
				$data['error'] = "Password and confirmation must be the same";
				$data['username']= $username;
				$data['email']= $email;
				$this->load->view('register',$data);
			}else{
				$result = $this->auth_model->insert($username,$password, $email);
				if($result){
					session_start();
					$_SESSION['logged_in'] = true;
					$_SESSION['username'] = $username;
					redirect('/');
				}else{
					$data['error'] = "Username already exists";
					$data['username']= $username;
					$data['email']= $email;
					$this->load->view('register',$data);
				}
			}
		}else{## si es la primera vez que la carga
			$this->load->view('register');
		}
		
	}

	public function login()
	{
		if(isset($_SESSION['logged_in'])){
			redirect('/');
		}
		if(isset($_POST["username"])){
			$username = $_POST['username'];
			$password = $_POST['password'];
			$userFound = $this->auth_model->verify($username,$password);	

			if($userFound->num_rows == 0){
				$data['error'] = "Username does not exists";
				$data['username']= $username;
				$this->load->view('login',$data);
			}else{
				session_start();
				$userFound = $userFound->result();
				//var_dump($userFound);
				$_SESSION['logged_in'] = true;
				$_SESSION['username'] = $username;
				$_SESSION['admin'] = $userFound[0]->Admin;
				//die;
				redirect('/');
			}

		}else{
			$this->load->view('login');
			
		}
	}

	public function logout(){
		session_destroy();
		session_unset();
		redirect('/');
	}
};