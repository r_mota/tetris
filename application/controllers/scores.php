<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Scores extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("select_model");
		session_start();
		if(!isset($_SESSION['logged_in'])){
			redirect('/auth/login');
		}
	}


	public function index(){
		//echo("index");
		$result['error'] = "";
		$result = $this->select_model->select();
		//$this->load->view('scores',$result);
		
		$data['result'] = $result;
		$data['username']= $_SESSION['username'];
		$data['admin']= $_SESSION['admin'];
		$this->load->view("scores",$data);

		
		//var_dump($result);
		//foreach ($result as $var){
		//var_dump($var);
		//}
	}

}

