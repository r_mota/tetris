context.font = "15px serif"
context.fillStyle = "#FF0000";
context.fillText ("Puntaje: "+PUNTAJE, 2, 15);
context.font = "15px serif"
context.fillStyle = "#FF0000";
context.fillText ("Nivel: " +NIVEL, 185, 15);

    function fillCells(t){
        for (var i = 0; i < t.coords().length; i++) {
            var y = t.coords()[i][0];
            var x = t.coords()[i][1]-1;
            if (x >= 0 && x >= 0)
                board[x][y] = tet.color;
			puntaje();
        }
    }

    function isOver(){
                    //drawTetromino(tet);
        for (var i = 0; i <board[0].length;i++){
            console.log(board[0][tet.x]);
            console.log(tet.x);
            if(board[0][tet.x] != 0){
                clearInterval(interval);//remove just to avoid repainting same object
                $('#start').text("Start new game");
                game_state = false;
                saveScore();
                pauseMusic();
                alert("Game Over");
                tet = null;  
				NIVEL=0;
				PUNTAJE=0;
				LINEASELIMINADAS=0;
				TIEMPO = 1000;
            }else{    
                tet = new Tetromino(Math.floor(Math.random() * (6 - 0 + 1)) +0);
                //tet = new Tetromino(0);
                //console.log(tet);
                drawTetromino(tet); 
            }
        }
    }

    function saveScore(){
    	if(PUNTAJE >0){
	    	var username = $('#username').val();
	    	var data = {'username':username, "score":PUNTAJE, "level":NIVEL};
	    	$.ajax({
			  type: "POST",
			  url: 'index.php/manager/',
			  data: {'data':data},
			  dataType: 'json'
			});
    	}
    }

    function deleteRows(){
        var rowsToDelete = [];
        var flag;
		var lines=0;
        for(var i = board.length-1; i>=0; i--){
            flag = true;
            for (var j=0; j< board[i].length;j++) {
               if(board[i][j] == 0){
                    flag = false;
                    break;
               }
            }
            if(flag){
                rowsToDelete.splice(0,0,i);
				lines=lines+1;
				LINEASELIMINADAS=LINEASELIMINADAS+1;
            }
        }
		NIVEL=Math.floor(parseFloat(LINEASELIMINADAS/5));
		if(NIVEL == 1)
		{
			TIEMPO=900;
			clearInterval(interval);
			interval = setInterval(function(){
      		tet.moveTetromino(1,1);
        },TIEMPO);
        }
		if(NIVEL == 2)
		{
			TIEMPO=800;
			clearInterval(interval);
			interval = setInterval(function(){
      		tet.moveTetromino(1,1);
        },TIEMPO);
        }
		if(NIVEL == 3)
		{
			TIEMPO=700;
			clearInterval(interval);
			interval = setInterval(function(){
      		tet.moveTetromino(1,1);
        },TIEMPO);
        }
		if(NIVEL == 4)
		{
			TIEMPO=600;
			clearInterval(interval);
			interval = setInterval(function(){
      		tet.moveTetromino(1,1);
        },TIEMPO);
        }
		if(NIVEL == 5)
		{
			TIEMPO=500;
			clearInterval(interval);
			interval = setInterval(function(){
      		tet.moveTetromino(1,1);
        },TIEMPO);
		}
		if(NIVEL == 6)
		{
			TIEMPO=400;
			clearInterval(interval);
			interval = setInterval(function(){
      		tet.moveTetromino(1,1);
        },TIEMPO);
        }
		if(NIVEL == 7)
		{
			TIEMPO=300;
			clearInterval(interval);
			interval = setInterval(function(){
      		tet.moveTetromino(1,1);
        },TIEMPO);
        }
		if(NIVEL == 8)
		{
			TIEMPO=200;
			clearInterval(interval);
			interval = setInterval(function(){
      		tet.moveTetromino(1,1);
        },TIEMPO);
        }
		if(NIVEL == 9)
		{
			TIEMPO=100;
			clearInterval(interval);
			interval = setInterval(function(){
      		tet.moveTetromino(1,1);
        },TIEMPO);
        }
		if(NIVEL == 0)
		{
			if(lines == 1)
			{
				PUNTAJE=PUNTAJE+40;
			}
			if(lines == 2)
			{
				PUNTAJE=PUNTAJE+100;
			}
			if(lines == 3)
			{
				PUNTAJE=PUNTAJE+300;
			}
			if(lines == 4)
			{
				PUNTAJE=PUNTAJE+1200;
			}
		}
		if(NIVEL > 0)
		{
			if(lines == 1)
			{
				PUNTAJE=PUNTAJE+40*(NIVEL+1);
			}
			if(lines == 2)
			{
				PUNTAJE=PUNTAJE+100*(NIVEL+1);
			}
			if(lines == 3)
			{
				PUNTAJE=PUNTAJE+300*(NIVEL+1);
			}
			if(lines == 4)
			{
				PUNTAJE=PUNTAJE+1200*(NIVEL+1);
			}
		}
        for(var i = 0; i<rowsToDelete.length;i++){
            board.splice(rowsToDelete[i],1);
            board.splice(0,0,Array.apply(null, new Array(GAME_COLS)).map(Number.prototype.valueOf,0));
            //drawTetromino(tet);
        }
        drawTetromino(tet);
    }
	function puntaje()
	{
		context.font = "15px serif"
		context.fillStyle = "#FF0000";
		context.fillText ("Puntaje: "+PUNTAJE, 2, 15);
		context.font = "15px serif"
		context.fillStyle = "#FF0000";
		context.fillText ("Nivel: " +NIVEL, 185, 15);
	}
