$( document ).ready(function(){
	var theme = null;
    $('.btn-primary').click(function(){
    	$('#save').html("Save");
		$('#save').removeClass("btn-success");
		var value = null;
    	$('#save').show();
		value = $(this).attr("value");
    	var canvas = document.getElementById("preview");	
    	var context = canvas.getContext("2d");
    	theme = value;
    	if(value != ""){
    		try{sound.pause()}catch(e){}
	    	var music = null;
	    	var image = new Image();
	    	music = value.substr(0,value.indexOf("."));
	    	image.src = "themes/image/"+value+"";
	    	setTimeout("",1000);
	    	context.drawImage(image, 0, 0);
			console.log('themes/music/'+music+".mp3");
			sound = new Audio('themes/music/'+music+".mp3");
			console.log('themes/music/'+music+".mp3");
			sound.play();
    	}
    	else{
    		try{sound.pause()}catch(e){}
    		context.fillStyle = "#000";
    		context.fillRect(0,0,canvas.width, canvas.height);
    		sound = new Audio('themes/music/1399848678.mp3');
			sound.play();
    	}
	}
    );

    $('#save').click(function(){
    	var data = {"username":$("#username").val(),"value":theme};
		$.ajax({
		  type: "POST",
		  url: 'manager/theme/',
		  data: {'data':data},
		  dataType: 'json'
		});
		$(this).html("Saved");
		$(this).addClass("btn-success");
    	}
    );
});