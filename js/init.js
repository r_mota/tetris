$('#start').click(function() {
    
    init();
        
});

function init() {
        if (game_state){
            clearInterval(interval);
            $('#start').text("start");
            game_state = false;
            pauseMusic();
            return;
        }
        game_state = true;
        $('#start').text("pause");

        //createWallpaper();

        playMusic();

        if(tet === null){
            board = createBoard(GAME_COLS, GAME_ROWS);
            tet = new Tetromino(Math.floor(Math.random() * (6 - 0 + 1)) +0);
        }

        
        drawTetromino(tet);
        interval = setInterval(function(){
      		tet.moveTetromino(1,1);
            drawTetromino(tet);
        },TIEMPO);    
}

function playMusic(){
    console.log("play function");

    if (!window.HTMLAudioElement) {
        //sound = new Audio('');
        return;
    }else{
        if(sound === null){
            
            sound = new Audio('');  
            var path = $('#path').val();
            path = path.replace("image","music");
            path = path.replace(" ","");
            path = path.substr(0,path.indexOf("."));
            console.log(path);
            if(sound.canPlayType('audio/mp3')) {
                if(path != "")
                    sound = new Audio(path+'.mp3');
                else
                    sound = new Audio('themes/music/1399848678.mp3');
               // console.log("mp3");
            }
            else if(sound.canPlayType('audio/ogg')) {
                sound = new Audio('sounds/music.ogg');
                //console.log("ogg");
            }
        }

        sound.play();
        sound.loop = true;
    }    
}

function pauseMusic(){
    sound.pause();
}

