function drawTetromino(t){
    if(!canvas.getContext){
        alert("no context");
    }
    
    fillBackground();
    context.fillStyle = t.color;
    var coordinates= t.coords();
    
    for (var i = 0; i<coordinates.length; i++) {        
        context.fillRect(coordinates[i][0]*BLOCK_SIZE+1,
            coordinates[i][1]*BLOCK_SIZE+1, 
            BLOCK_SIZE-1, BLOCK_SIZE-1);
    }
}

function fillBackground(){
    var flag = true
    context.fillStyle = "#000";
    context.fillRect(0,0,canvas.width, canvas.height);
    if(wallpaper != null){
        context.drawImage(wallpaper, 0, 0);
    }else{
        createWallpaper();
    }
    /*wallpaper = new Image();
    wallpaper.src = "images/wallpapers/wall2.png";
    wallpaper.onload = function(){
        context.drawImage(wallpaper, 0, 0);
        context.font = "15px serif"
        context.fillStyle = "#FF0000";
        context.fillText ("Puntaje: "+PUNTAJE, 2, 15);
        context.font = "15px serif"
        context.fillStyle = "#FF0000";
        context.fillText ("Nivel: " +NIVEL, 185, 15);
    }*/
    context.drawImage(wallpaper, 0, 0);
    for (var i = board.length - 1; i >= 0; i--) {// Y

        for (var j = 0; j< board[0].length;j++) {//X
            
            if (board[i][j] != 0){
                context.fillStyle = board[i][j];
                context.fillRect(j*BLOCK_SIZE+1,
                    i*BLOCK_SIZE+1, 
                    BLOCK_SIZE-1, BLOCK_SIZE-1);
        
                flag = true;
        
            }else{
        
                flag = false;
        
            }

        }
    }
    context.font = "15px serif"
    context.fillStyle = "#FF0000";
    context.fillText ("Puntaje: "+PUNTAJE, 2, 15);
    context.font = "15px serif"
    context.fillStyle = "#FF0000";
    context.fillText ("Nivel: " +NIVEL, 185, 15);

	//puntaje();
}

function createWallpaper(){
    var path = $('#path').val();    
    wallpaper = new Image();
    wallpaper.src = path;
    wallpaper.onload = function(){
        context.drawImage(wallpaper, 0, 0);
        context.font = "15px serif"
        context.fillStyle = "#FF0000";
        context.fillText ("Puntaje: "+PUNTAJE, 2, 15);
        context.font = "15px serif"
        context.fillStyle = "#FF0000";
        context.fillText ("Nivel: " +NIVEL, 185, 15);
    }

}

fillBackground();
