/**Class Tetromino**/    
var Tetromino = function(type) {
    this.type = type;
    this.rotation = 0;

    this.x = (GAME_COLS / 2) - 1;
    this.y = -1;
    
    var generateRotations = function(coords, center) {
            var result = [];
            result.push(coords);
            var last = coords;
            for (var i = 0; i < 3; ++i) {
                 last = rotate90(last, center);
                 result.push(last);
            }
            return result;
        }
    


    var tetrominoColors = [ '#0ff', '#f00', '#0f0', '#ff0', '#f0f', '#00f', '#f50' ];

    switch (type) {
    case 0:
        //this.rotations = generateRotations([[-1, 0], [0, 0], [1, 0], [2, 0]], [ 0.5, 0.5 ]);
        this.rotations = generateRotations([[-1, 0], [0, 0], [1, 0], [2, 0]], [ 0.5, 0.5 ]);
        //this.rotation = 2;
        this.color = tetrominoColors[0];
        break;
    case 1:
        this.rotations = generateRotations([[-1, -1], [0, -1], [0, 0], [1, 0]]);
        this.color = tetrominoColors[1];
        break;
    case 2:
        this.rotations =  generateRotations([[-1, 0], [0, 0], [0, -1], [1, -1]]);
        this.color = tetrominoColors[2];
        break;
    case 3:
        this.rotations = [[[0, 0], [0, 1], [1, 0], [1, 1]]];
        this.color = tetrominoColors[3];
        break;
    case 4:
        this.rotations = generateRotations([[0, 0], [-1, 0], [1, 0], [0, -1]]);
        this.color = tetrominoColors[4];
        break;
    case 5:
        this.rotations = generateRotations([[-1, -1], [-1, 0], [0, 0], [1, 0]]);
        this.color = tetrominoColors[5];
        break;
    case 6:
        this.rotations = generateRotations([[-1, 0], [0, 0], [1, 0], [1, -1]]);
        this.color = tetrominoColors[6];
        break;
    }

    for (var i = 0, len = this.rotations.length; i < len; ++i) {
        this.rotations[i].sort(comparePoints);
    }

    this.coords = function() {
        var delta_x = this.x;
        var delta_y = this.y;
        var result = map(this.rotations[this.rotation],
            function(pos) { return [ pos[0] + delta_x, pos[1] + delta_y ]; }); 
        return result;
    } 
     this.bounds = function() {
        var c = this.coords();
        var min = [ c[0][0], c[0][1] ];
        var max = [ c[0][0], c[0][1] ];

        c.slice(1).forEach(
            function(pos) {
                min = [ Math.min(min[0], pos[0]), Math.min(min[1], pos[1]) ];
                max = [ Math.max(max[0], pos[0]), Math.max(max[1], pos[1]) ];
            });
        return [ min, max ];
    }

    this.center = function() {
        var bounds = this.bounds();
        return [ Math.ceil((bounds[0][0] + bounds[1][0]) / 2),
                 Math.ceil((bounds[0][1] + bounds[1][1]) / 2) ];
    }

    this.cloned = function() {
        var result = new Tetromino(this.type);
        result.x = this.x;
        result.y = this.y;
        result.rotation = this.rotation;
        return result;
    }

    this.translated = function(delta) {
        var result = this.cloned();
        result.x = this.x + delta[0];
        result.y = this.y + delta[1];
        return result;
    } 
    
    var comparePoints = function(a, b) {
        if (a[0] < b[0])
            return -1;
        if (a[0] > b[0])
            return 1;
        if (a[1] < b[1])
            return -1;
        if (a[1] > b[1])
            return 1;
        return 0;
    }

    function map(array, func) {
        var result = [];
        for (var i = 0, len = array.length; i < len; ++i) {
            result.push(func(array[i]));
        }
        return result;
    }

    function rotate90(tetromino, center) {
        return map(tetromino, function (pos) {
            if (center === undefined)
                center = [0, 0];
            return [(pos[1] - center[1]) + center[1], -(pos[0] - center[0]) + center[1]];
        });} 

   this.rotate = function(spin) {
            if(this.type == 3){
                return;
            }
            if(this.type == 0){
                if (this.rotation == 1){
                    this.rotation = 0;
                }else if(this.rotation == 0){
                    this.rotation=1;
                }
                return;
            }

            if(this.rotation == 0 ){
                if (spin == 1){
                    this.rotation = 3;
                }else{
                    this.rotation ++;
                }
            }else 
                if(this.rotation == 3){
                    if (spin == 1){
                        this.rotation --;
                    }else{
                        this.rotation = 0;
                    }
                }else{
                    if(spin == 1){
                        this.rotation --;
                    }else{
                        this.rotation ++;
                    }

                }
    }

    this.moveTetromino= function(type ,num){//will take 3 arguments: tetromino, type of move  (0-side move, 1-drop), and -1 if is to left.
        var valid = true;
        switch(type){
            case 0:
                this.x = this.x + num;
                for (var i = 0; i < this.coords().length; i++) {
                    if(this.coords()[i][1]<0){
                       continue;
                    }
                    var x = this.coords()[i][1];
                    var y = this.coords()[i][0];  
                    if (y > 9 || y < 0 || board[x][y]!=0){
                        this.x = this.x - num;
                        break;
                    }
                };
                drawTetromino(this);
                break;
            case 1:

                this.y +=1;
                for (var i = 0; i < this.coords().length; i++) {
                    //console.log(this.coords());
                    if(this.coords()[i][1]<0){
                        drawTetromino(this);
                        continue;
                    }
                    var x = this.coords()[i][1];
                    var y = this.coords()[i][0];  
                    try{
                        if (x > 19 || board[x][y] != 0){
                            fillCells(this);
                            this.y -=1
                            deleteRows();

                            isOver();
                            //tet = null;
                            break;
                        }
                    }catch(e){
                        this.y-=1;
                        break;
                    }    
                };
                drawTetromino(this);
                break;
            case 2:
                this.rotate(num);
                for (var i = 0; i < this.coords().length; i++) {
                    var x = this.coords()[i][1];
                    var y = this.coords()[i][0];
                    if(x === undefined || y === undefined){
                        valid = false;
                        break;
                    }
                    try{
                        if (y > 9 || y < 0 || x > 19 ||board[x][y]!=0){
                            this.rotate(num*-1);
                            break;
                        }
                    }catch(e){
                        this.rotate(num*-1);
                        break;
                    }  
                }
                drawTetromino(this);
                break;   
            default:
                break;    
        }
        return valid;       
    }
}