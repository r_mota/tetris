 /**Constants**/

var GAME_COLS = 10;
var GAME_ROWS = 20
var KEY_LEFT = 37;
var KEY_RIGHT = 39;
var KEY_UP = 38;
var KEY_DOWN = 40;
var PUNTAJE = 0;
var NIVEL = 0;
var LINEASELIMINADAS = 0;
var BLOCK_SIZE = 25;
var TIEMPO = 1000;

/**Global variables**/
var game_state = false;
var music_state = false;
var board = createBoard(GAME_COLS, GAME_ROWS);
var canvas = document.getElementById("board");
var context = canvas.getContext("2d");
var tet = null;
var sound = null;
var wallpaper = null;

function createBoard(width, height){// create a matrix reprensenting the game board
    var rows = new Array(height);
    for (var i = 0; i <rows.length; i++) {
        rows[i] = Array.apply(null, new Array(width)).map(Number.prototype.valueOf,0);
    };

    return rows;
}
